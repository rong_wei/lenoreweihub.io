define([], function(){

	var Tips = (function(){

		var $tipBox = $(".tips-box");

		return {
			show: function(){
				$tipBox.slideDown();
			},
			hide: function(){
				$tipBox.slideUp();
			},
			init: function(){
				
			}
		}
	})();

	var resetTags = function(){
		var tags = $(".tagcloud a");
		tags.css({"font-size": "12px"});
		for(var i=0,len=tags.length; i<len; i++){
			var num = tags.eq(i).html().length % 5 +1;
			tags[i].className = "";
			tags.eq(i).addClass("color"+num);
		}
	}

	var slide = function(idx){
		var $wrap = $(".switch-wrap");
		$wrap.css({
			"transform": "translate(-"+idx*100+"%, 0 )"
		});
		$(".icon-wrap").addClass("hide");
		$(".icon-wrap").eq(idx).removeClass("hide");
	}
	$(".hidebtn").click(function() {
		if($(".mid-col").css("left")=="300px"){
			$(".left-col").animate({"margin-left": '-=300'});
			$(".mid-col").animate({"left": '-=300'});
		}else{
			$(".left-col").animate({"margin-left": '+=300'});
			$(".mid-col").animate({"left": '+=300'});	
			}
		$('.middlebtn').toggleClass('hidemiddle');
    	$('.bottombtn').toggleClass('rotateup');
    	$('.topbtn').toggleClass('rotatedown');	
	});
	var bind = function(){
		var switchBtn = $("#myonoffswitch");
		var tagcloud = $(".second-part");
		var navDiv = $(".first-part");
		switchBtn.click(function(){
			if(switchBtn.hasClass("clicked")){
				switchBtn.removeClass("clicked");
				tagcloud.removeClass("turn-left");
				navDiv.removeClass("turn-left");
			}else{
				switchBtn.addClass("clicked");
				tagcloud.addClass("turn-left");
				navDiv.addClass("turn-left");
				resetTags();
			}
		});

		var timeout;
		var isEnterBtn = false;
		var isEnterTips = false;

		
		$(".icon").click(function() {
			if (!isEnterBtn) {
				isEnterBtn = true;
				Tips.show();}
			else{
				setTimeout(function(){
						Tips.hide();
						isEnterBtn = false;
				}, 100);}
		});
		$(".tips-box").click(function() {
			if (!isEnterBtn) {
				isEnterBtn = true;
				Tips.show();}
			else{
				setTimeout(function(){
						Tips.hide();
						isEnterBtn = false;
				}, 100);}
		});

		$(".tips-inner li").bind("click", function(){
			var idx = $(this).index();
			slide(idx);
			Tips.hide();
		});
	}

	

	return {
		init: function(){
			resetTags();
			bind();
			Tips.init();
		}
	}
});